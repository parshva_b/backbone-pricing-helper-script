## Debugging v6 price request

The purpose of this repo is to convert json to and from backbone v6 requests to key-value pair

1. /v6req -> transforms v6 request to key value pair
2. /kv2backbone -> key-value pair to v6 request

| route        | sample file      |
| ------------ | ---------------- |
| /v6req       | v6req.rest       |
| /kv2backbone | kv2backbone.rest |

### How to use?

1. run `npm i` locally on first install
2. run `node index` to start server
3. run requests from `requests` folder

### Requirements

1. VSCode
2. node, npm
3. VSCode REST client extension
