 const express = require('express')
const { isEmpty, get } = require('lodash')
const app = express()
app.use(express.json())

const { checkReq, convertToPim, transformToKV, transformFromKV } = require('./util')

app.post('/v6req', (req, res) => {
	const body = req.body
	if(checkReq(body, ['variableAttributes'])) {
		let selection = get(body, 'variableAttributes')
		const isInPim = get(body, 'pimAttributesInRequest')
		if(isEmpty(isInPim) && !isInPim) {
			//convert from ___LOV to normal
			selection = convertToPim(selection);
		}
		
		const transformedSelection = transformToKV(selection)
		res.status(200).json({ ...transformedSelection }).send()

	} else {
		res.status(400).json({ message: 'Please check body' }).send()
	}
})

app.post('/kv2backbone', (req, res) => {
	const body = req.body
	const variableAttributes = transformFromKV(body)
	res.status(200).json({ variableAttributes }).send()
})

app.listen(8080, () => console.log(`Server started`))