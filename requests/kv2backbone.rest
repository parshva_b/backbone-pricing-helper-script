POST http://localhost:8080/kv2backbone
Content-Type: application/json

{
    "Type Of Exhibition Wall": "Pop Up",
    "Format": "Pop Up Medium (301 X 224 Cm)",
    "Exhibition System": "Complete System",
    "Printing Colors": "4/0 Full Color",
    "Printing Process": "Digital",
    "Delivery Type": "Normal"
}

# {
# 	key: value
# }