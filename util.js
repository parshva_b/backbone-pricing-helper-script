const { get, isEmpty } = require('lodash')

const checkReq = (req, arr) => {
	if(Array.isArray(arr)) {
		if(arr.length === 0)return true;
		
		arr.forEach(entry => {
			if(isEmpty(get(req, entry))) return false;
		})
		return true;
	};
	return true;
}

const convertToPim = (selection) => {
	const map = new Map()

	selection.forEach(sel => {
		const name = get(sel, 'name')
		const value = get(sel, 'value')

		const cName = name.substring(0, name.indexOf('___')).split('_').join(' ')
		map.set(cName, value)
	})

	return Object.fromEntries(map);
}

const transformToKV = (selection) => {
	const map = new Map();
	selection.forEach(sel => map.set(get(sel, 'name'),get(sel, 'value')))
	return Object.fromEntries(map)
}

const transformFromKV = (selection) => {
	const arr = []
	for(let [key, value] of Object.entries(selection)) {
		arr.push({
			name: key,
			value: value
		})
	}

	return arr;
}

module.exports = { checkReq, convertToPim, transformToKV, transformFromKV }